#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>

#include <algorithm>
#include <chrono>
#include <memory>
#include <thread>
#include <vector>

#include "Dareman.h"
#include "Ghost.h"
#include "Level.h"
#include "Renderer.h"
#include "Sprite.h"

#define BLACK 0x000000FF
#define YELLOW 0xFFFF00FF

static SDL_Window* sWindow = nullptr;

enum GameState
{
	Run,
	Quit,
	Error,
	Win,
	Loss
};

bool InitApp()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
		return false;

	if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
		return false;

	if (TTF_Init() != 0)
		return false;

	sWindow = SDL_CreateWindow("Dareman", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, 0);
	if (sWindow == nullptr)
		return false;

	return true;
}

void ShutdownApp()
{
	if (sWindow)
		SDL_DestroyWindow(sWindow);

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

// Shows a modal dialog that is hidden on keypress
// Returns false if ESC was pressed, true if any other key was pressed
bool ShowDialog(Renderer* aRenderer, TTF_Font* aTitleFont, TTF_Font* aTextFont, const char* aTitle, const char* aText)
{
	const int spacing = 20;

	int titleWidth, titleHeight, textWidth, textHeight;
	TTF_SizeText(aTitleFont, aTitle, &titleWidth, &titleHeight);
	TTF_SizeText(aTitleFont, aTitle, &textWidth, &textHeight);

	const int dialogWidth = spacing * 2 + std::max(titleWidth, textWidth);
	const int dialogHeight = spacing * 3 + titleHeight + textHeight;

	Sprite* background = aRenderer->CreateRGBSprite(dialogWidth, dialogHeight, BLACK);

	int windowWidth, windowHeight;
	SDL_GetWindowSize(sWindow, &windowWidth, &windowHeight);

	const int dialogX = windowWidth / 2 - dialogWidth / 2;
	const int dialogY = windowHeight / 2 - dialogHeight / 2;

	aRenderer->DrawSprite(background, dialogX, dialogY);
	aRenderer->DrawText(aTitleFont, aTitle, YELLOW, dialogX + dialogWidth / 2 - titleWidth / 2, dialogY + spacing);
	aRenderer->DrawText(aTextFont, aText, YELLOW, dialogX + dialogWidth / 2 - textWidth / 2, dialogY + spacing * 2 + titleHeight);
	aRenderer->EndFrame();

	// Modal event loop
	for (;;)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE: return false;
				default: return true;
				}
			}
			else if (event.type == SDL_QUIT)
			{
				return false;
			}
		}
	}

	return false;
}

void ProcessInput(GameState& gameState, Direction& playerInput)
{
	// Handle input
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE: gameState = Quit; break;
			case SDLK_LEFT: playerInput = Left; break;
			case SDLK_RIGHT: playerInput = Right; break;
			case SDLK_UP: playerInput = Up; break;
			case SDLK_DOWN: playerInput = Down; break;
			default:
				// Ignore other keys
				break;
			}
		}
		else if (event.type == SDL_KEYUP)
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_LEFT:
			case SDLK_RIGHT:
			case SDLK_UP:
			case SDLK_DOWN: playerInput = None; break;
			default:
				// Ignore other keys
				break;
			}
		}
		else if (event.type == SDL_QUIT)
		{
			gameState = Quit;
		}
	}
}

void MoveDareman(Dareman* dareman, Direction playerInput, const float deltaSeconds)
{
	if (dareman->IsOnTile())
	{
		if (playerInput == None || !dareman->CanMove(playerInput))
		{
			dareman->mDirection = None;
		}
		else
		{
			dareman->mDirection = playerInput;
		}
	}

	bool bMoveRequested = dareman->mDirection != None;
	if (bMoveRequested)
	{
		dareman->MoveToNextTile(deltaSeconds);
	}
}

void UpdateDareman(Dareman* aDareman, Direction aPlayerInput, const float aDeltaSeconds)
{
	aDareman->UpdateIncibleFrame(aDeltaSeconds);
	MoveDareman(aDareman, aPlayerInput, aDeltaSeconds);
}

void FleeFromDareman(Dareman* aDareman, std::unique_ptr<Ghost>& aGhost, Level* aLevel)
{
	Position daremanPos = aDareman->GetLevelPosition();
	Position ghostPos = aGhost->GetLevelPosition();
	Direction furthestDirFromDareman = None;
	float expectedSquaredDist = (ghostPos - daremanPos).SquaredLength();
	constexpr Direction dirsToTest[4] = {Direction::Down, Direction::Left, Direction::Right, Direction::Up};
	for (Direction dirToTest : dirsToTest)
	{
		Position NextPos = ghostPos;

		switch (dirToTest)
		{
		case Up: --NextPos.Y; break;
		case Down: ++NextPos.Y; break;
		case Left: --NextPos.X; break;
		case Right: ++NextPos.X; break;
		default: break;
		}
		float squaredDistToNextPos = (NextPos - daremanPos).SquaredLength();
		const Tile& tile = aLevel->GetTile(NextPos.X, NextPos.Y);
		if (expectedSquaredDist < squaredDistToNextPos && tile.mCollision == Collision::None)
		{
			expectedSquaredDist = squaredDistToNextPos;
			furthestDirFromDareman = dirToTest;
		}
	}
	aGhost->mDirection = furthestDirFromDareman;
}

void FollowDareman(Dareman* aDareman, std::unique_ptr<Ghost>& aGhost, Level* aLevel)
{
	Position daremanPos = aDareman->GetLevelPosition();
	Position ghostPos = aGhost->GetLevelPosition();
	auto&& Moves = aLevel->ComputeShortestPath(ghostPos.X, ghostPos.Y, daremanPos.X, daremanPos.Y);
	aGhost->mDirection = None;
	if (!Moves.empty() && aGhost->CanMove(Moves[0]))
	{
		aGhost->mDirection = Moves[0];
	}
}

void UpdateGhostDirection(Dareman* aDareman, std::unique_ptr<Ghost>& aGhost, Level* aLevel)
{
	if (aGhost->IsOnTile())
	{
		if (aDareman->IsInvicible())
		{
			FleeFromDareman(aDareman, aGhost, aLevel);
		}
		else
		{
			FollowDareman(aDareman, aGhost, aLevel);
		}
	}
}

void MoveGhost(std::unique_ptr<Ghost>& aGhost, const float aDeltaSeconds)
{
	if (aGhost->mDirection != None)
	{
		aGhost->MoveToNextTile(aDeltaSeconds);
	}
}

void UpdateGhosts(Dareman* aDareman, std::vector<std::unique_ptr<Ghost>>& aGhosts, Level* aLevel, const float aDeltaSeconds)
{
	// Process ghosts
	for (auto&& ghost : aGhosts)
	{
		ghost->SetFleeing(aDareman->IsInvicible());
		UpdateGhostDirection(aDareman, ghost, aLevel);
		MoveGhost(ghost, aDeltaSeconds);
	}
}

GameState ProcessDaremanAndGhostCollision(Dareman* aDareman, std::vector<std::unique_ptr<Ghost>>& aGhosts, GameState aGameState)
{
	// Process ghost/player collision
	Position daremanPos = aDareman->GetLevelPosition();
	for (auto&& ghost : aGhosts)
	{
		if (ghost->GetLevelPosition() == daremanPos)
		{
			if (aDareman->IsInvicible())
			{
				ghost->MarkAsToBeKilled();
			}
			else
			{
				aGameState = GameState::Loss;
			}
		}
	}
	return aGameState;
}

void ProcessPickup(Dareman* aDareman, int& aPlayerScore, Level* aLevel, GameState& aGameState)
{
	// Process Pickups
	Position daremanPos = aDareman->GetLevelPosition();
	if (aDareman->IsOnTile())
	{
		Pickup pickup = aLevel->TryToPickupTile(daremanPos.X, daremanPos.Y);
		switch (pickup)
		{
		case Pickup::Big: aDareman->BecomeInvicible(); break;
		case Pickup::Small: ++aPlayerScore; break;
		case Pickup::None: break;
		}
		if (aLevel->GetPickupCount() == 0)
		{
			aGameState = GameState::Win;
		}
	}
}

void ProcessPendingToBeKilled(std::vector<std::unique_ptr<Ghost>>& aGhosts)
{
	aGhosts.erase(
		std::remove_if(aGhosts.begin(), aGhosts.end(), [](std::unique_ptr<Ghost> const& ghost) { return ghost->IsPendingToBeKilled(); }),
		aGhosts.end());
}

int main(int argc, char* argv[])
{
	if (!InitApp())
		return 1;

	Renderer* renderer = new Renderer(sWindow);

	Level* level = new Level();
	level->LoadLevel("Data/Level/");

	if (!level->IsValid())
	{
		ShutdownApp();
		return 1;
	}

	TTF_Font* openSansFont = TTF_OpenFont("Data/Fonts/OpenSans-Regular.ttf", 16);
	TTF_Font* pacmanFont = TTF_OpenFont("Data/Fonts/Pac-Font.ttf", 20);

	int titleWidth, titleHeight, scoreLabelWidth, scoreLabelHeight, fpsLabelWidth, fpsLabelHeight;
	TTF_SizeText(pacmanFont, "dareman", &titleWidth, &titleHeight);
	TTF_SizeText(openSansFont, "Score: 000", &scoreLabelWidth, &scoreLabelHeight);
	TTF_SizeText(openSansFont, "FPS: 000.0", &fpsLabelWidth, &fpsLabelHeight);

	const int spacing = 4;
	const int headerHeight = std::max(titleHeight, fpsLabelHeight) + spacing * 2;
	const int scoreLabelX = level->GetWidthPx() / 2 - scoreLabelWidth / 2;
	const int fpsLabelX = level->GetWidthPx() - spacing - fpsLabelWidth;

	renderer->SetOffset(0, headerHeight);

	// Adjust window to fit the level
	SDL_SetWindowSize(sWindow, level->GetWidthPx(), level->GetHeightPx() + headerHeight);
	SDL_SetWindowPosition(sWindow, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);

	// Create game entities
	Dareman* dareman = new Dareman();
	const auto& daremanPosition = level->GetStartPosition(ECharacter::Dareman);
	dareman->SetPosition(daremanPosition.first * TILE_SIZE, daremanPosition.second * TILE_SIZE);

	std::vector<std::unique_ptr<Ghost>> ghosts;

	ghosts.emplace_back(std::make_unique<Ghost>(ECharacter::Blinky));
	ghosts.emplace_back(std::make_unique<Ghost>(ECharacter::Inky));
	ghosts.emplace_back(std::make_unique<Ghost>(ECharacter::Pinky));
	ghosts.emplace_back(std::make_unique<Ghost>(ECharacter::Clyde));

	// Startup dialog
	renderer->BeginFrame();
	renderer->SetOffset(0, headerHeight);
	level->Render(renderer);
	ShowDialog(renderer, pacmanFont, openSansFont, "dareman", "Press a key to start");

	// Main application loop
	auto now = std::chrono::high_resolution_clock::now();
	auto lastFrame = now;
	GameState gameState = Run;
	Direction playerInput = None;
	int PlayerScore = 0;

	while (gameState == Run)
	{
		now = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::milli> deltaTime = now - lastFrame;
		lastFrame = now;

		ProcessInput(gameState, playerInput);

		// Limit FPS to 100, do not remove this as a means of optimization
		if (deltaTime.count() < 10)
		{
			std::chrono::duration<double, std::milli> sleepMs(10 - deltaTime.count());
			std::this_thread::sleep_for(std::chrono::duration_cast<std::chrono::milliseconds>(sleepMs));
			deltaTime = deltaTime + sleepMs;
		}

		// Update game state
		if (gameState == Run)
		{
			const float deltaSeconds = float(deltaTime.count()) / 1000.f;

			UpdateDareman(dareman, playerInput, deltaSeconds);

			ProcessPickup(dareman, PlayerScore, level, gameState);

			UpdateGhosts(dareman, ghosts, level, deltaSeconds);

			gameState = ProcessDaremanAndGhostCollision(dareman, ghosts, gameState);

			ProcessPendingToBeKilled(ghosts);
		}
		// Render
		renderer->BeginFrame();
		renderer->SetOffset(0, headerHeight);

		level->Render(renderer);

		const float deltaSeconds = float(deltaTime.count()) / 1000.f;
		dareman->Render(renderer, deltaSeconds);
		for (auto&& ghost : ghosts)
		{
			ghost->Render(renderer);
		}

		// Rendering Title and FPS
		renderer->SetOffset(0, 0);
		renderer->DrawText(pacmanFont, "dareman", YELLOW, spacing, spacing);

		char scoreLabel[15];
		sprintf(scoreLabel, "Score: %3i", PlayerScore);
		renderer->DrawText(openSansFont, scoreLabel, YELLOW, scoreLabelX, spacing);

		char fpsLabel[15];
		sprintf(fpsLabel, "FPS: %5.1f", 1000.f / float(deltaTime.count()));
		renderer->DrawText(openSansFont, fpsLabel, YELLOW, fpsLabelX, spacing);

		renderer->EndFrame();
	}

	if (gameState == Win)
		ShowDialog(renderer, pacmanFont, openSansFont, "you win !", "Congratulations");
	else if (gameState == Loss)
		ShowDialog(renderer, pacmanFont, openSansFont, "you lose!", "Try again");

	// Cleanup
	delete dareman;
	delete level;
	delete renderer;

	ShutdownApp();
	return gameState == Error ? 1 : 0;
}
