#pragma once

#include "Type.h"

#define TILE_SIZE 24
#define GAMEOBJECT_SPEED (TILE_SIZE * 6)

enum Direction
{
	Up,
	Down,
	Left,
	Right,
	None
};

class Character
{
public:
	Character();
	virtual ~Character();

	void SetPosition(int aPosX, int aPosY);
        Position GetLevelPosition() const;

	virtual bool CanMove(Direction aDirection) const;
	bool IsOnTile() const;

	// Moves the character to the "next" tile.
	// If Dareman is not already on one tile, it finishes its current movement,
	// if it is on a tile it moves to the next tile.
	// Returns delta time remaining after the movement
	float MoveToNextTile(float aDeltaTime);

        void MarkAsToBeKilled(); 
        bool IsPendingToBeKilled() const;

	Direction mDirection;

protected:
	float mPosX;
	float mPosY;

        bool mPendingToBeKilled;
};
