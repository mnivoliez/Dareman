#include "Level.h"

#include <algorithm>
#include <cassert>
#include <queue>
#include <string>
#include <vector>

#include "Renderer.h"
#include "Sprite.h"
#include "Type.h"

#define MAKEID(char1, char2) char1 + (char2 << 8)

namespace
{
std::vector<Direction> ReconstructPath(std::map<Position, Position> const& CameFrom, Position const& InCurrent)
{
	std::vector<Direction> Result;
	Direction LastDirRegister = None;
	Position Current = InCurrent;
	auto It = CameFrom.find(Current);
	while (It != CameFrom.end())
	{
		Position Move = Current - It->second;
		Direction DirToAdd = None;
		if (Move.X == 1)
		{
			DirToAdd = Right;
		}
		if (Move.X == -1)
		{
			DirToAdd = Left;
		}
		if (Move.Y == 1)
		{
			DirToAdd = Down;
		}
		if (Move.Y == -1)
		{
			DirToAdd = Up;
		}
		if (DirToAdd != LastDirRegister)
		{
			LastDirRegister = DirToAdd;
			Result.push_back(DirToAdd);
		}
		Current = It->second;
		It = CameFrom.find(Current);
	}
	// We need to reverse the array as we start from last move
	std::reverse(Result.begin(), Result.end());
	return Result;
}

// Modulo that handle negative value
int Mod(int a, int b)
{
	return a >= 0 ? a % b : (b - abs(a % b)) % b;
}
} // namespace

Level* Level::sInstance = nullptr;

Level::Level()
	: mWidth(0)
	, mHeight(0)
	, mPickupCount(0)
{
	sInstance = this;
}

Level::~Level() {}

void Level::LoadLevel(const char* aPath)
{
	std::string rootPath = aPath;
	LoadLevelFile((rootPath + "Level.txt").c_str());
	LoadPickupFile((rootPath + "Pickups.txt").c_str());
}

void Level::LoadLevelFile(const char* aPath)
{
	mTiles.clear();

	// The data is expected to be properly formatted
	// All lines should be the same length and consisting of series of two characters separated by spaces
	FILE* file = fopen(aPath, "r");

	char tileId[3] = "00";
	char delimiter;
	mTiles.emplace_back();
	while (fscanf(file, "%2c%c", &tileId, &delimiter) != EOF)
	{
		short ID = MAKEID(tileId[0], tileId[1]);
		Tile tile = {ID, Collision::CollidesAll, Pickup::None};
		if (tileId[1] == '0')
		{
			if (tileId[0] == '0')
				tile.mCollision = Collision::None;
			else
				tile.mCollision = Collision::CollidesPlayer;
		}

		mTiles.back().push_back(tile);

		if (delimiter == '\n')
			mTiles.emplace_back();
	}

	fclose(file);

	// Check integrity of the level
	mWidth = (int)mTiles[0].size();
	mHeight = (int)mTiles.size();
}

void Level::LoadPickupFile(const char* aPath)
{
	mPickupCount = 0;
	mStartPositions.clear();

	// The data is expected to be properly formatted and fitting the Level.txt
	FILE* file = fopen(aPath, "r");

	char pickupType;
	char delimiter;
	int col = 0;
	int row = 0;

	while (fscanf(file, "%c%c", &pickupType, &delimiter) != EOF)
	{
		switch (pickupType)
		{
		case '1':
			mPickupCount++;
			mTiles[row][col].mPickup = Pickup::Small;
			break;
		case '2':
			mPickupCount++;
			mTiles[row][col].mPickup = Pickup::Big;
			break;
		case 'D':
		case 'B':
		case 'I':
		case 'P':
		case 'C': mStartPositions[(ECharacter)pickupType] = std::make_pair(col, row); break;
		default: break;
		}

		if (delimiter == '\n')
		{
			row++;
			col = 0;
		}
		else
		{
			col++;
		}
	}

	fclose(file);

	// Check integrity of the level
	mWidth = (int)mTiles[0].size();
	mHeight = (int)mTiles.size();
}

bool Level::IsValid() const
{
	return mTiles.size() > 0 && mPickupCount > 0 && mStartPositions.size() == 5;
}

void Level::Render(Renderer* aRenderer) const
{
	for (int row = 0; row < mHeight; row++)
	{
		for (int col = 0; col < mWidth; col++)
		{
			const auto& tile = mTiles[row][col];

			Sprite* sprite = nullptr;
			switch (tile.mTileId)
			{
			case MAKEID('c', '1'):
				sprite = aRenderer->GetSprite("Data/Images/c1.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('c', '2'):
				sprite = aRenderer->GetSprite("Data/Images/c2.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('c', '3'):
				sprite = aRenderer->GetSprite("Data/Images/c3.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('c', '4'):
				sprite = aRenderer->GetSprite("Data/Images/c4.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('d', '1'):
				sprite = aRenderer->GetSprite("Data/Images/d1.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('d', '2'):
				sprite = aRenderer->GetSprite("Data/Images/d2.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('d', '3'):
				sprite = aRenderer->GetSprite("Data/Images/d3.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('d', '4'):
				sprite = aRenderer->GetSprite("Data/Images/d4.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('w', '1'):
				sprite = aRenderer->GetSprite("Data/Images/w1.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('w', '2'):
				sprite = aRenderer->GetSprite("Data/Images/w2.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('w', '3'):
				sprite = aRenderer->GetSprite("Data/Images/w3.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('w', '4'):
				sprite = aRenderer->GetSprite("Data/Images/w4.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('f', 'f'):
				sprite = aRenderer->GetSprite("Data/Images/ff.png");
				sprite->SetSize(TILE_SIZE, TILE_SIZE);
				break;
			case MAKEID('0', '0'):
				switch (tile.mPickup)
				{
				case Pickup::Small:
					sprite = aRenderer->GetSprite("Data/Images/pickup_small.png");
					sprite->SetSize(TILE_SIZE, TILE_SIZE);
					break;
				case Pickup::Big:
					sprite = aRenderer->GetSprite("Data/Images/pickup_big.png");
					sprite->SetSize(TILE_SIZE, TILE_SIZE);
					break;
				default: break;
				}
				break;
			case MAKEID('f', '0'):
			default: break;
			}

			if (sprite)
				aRenderer->DrawSprite(sprite, col * TILE_SIZE, row * TILE_SIZE);
		}
	}
}

const Tile& Level::GetTile(int aCol, int aRow) const
{
	return mTiles[aRow][aCol];
}

const Tile& Level::GetNextTile(int aCol, int aRow, Direction aDirection) const
{
	switch (aDirection)
	{
	case Up: return GetTile(aCol, aRow - 1);
	case Down: return GetTile(aCol, aRow + 1);
	case Right: return GetTile(Mod((aCol + 1), mWidth), aRow);
	case Left: return GetTile(Mod((aCol - 1), mWidth), aRow);
	default:
		assert(false); // This should not happen
		return mTiles[0][0];
	}
}

Pickup Level::TryToPickupTile(int aCol, int aRow)
{
	Tile& tile = mTiles[aRow][aCol];
	Pickup Result = tile.mPickup;
	switch (tile.mPickup)
	{
	case Pickup::Big:
	case Pickup::Small: --mPickupCount; break;
	default: break;
	}
	tile.mPickup = Pickup::None;
	return Result;
}

std::vector<Direction> Level::ComputeShortestPath(int aStartCol, int aStartRow, int aDestCol, int aDestRow) const
{
	// our openset will have a list of position ordered from the probably cheapest to the probably most expansive
	// that way we make our chances to find the cheapest faster greater
	typedef std::pair<float, Position> OpenCandidate;
	struct OpenCandidateComparator
	{
		bool operator()(const OpenCandidate& l, const OpenCandidate& r) { return l.first > r.first; }
	};

	Position Start = {aStartCol, aStartRow};
	Position Dest = {aDestCol, aDestRow};

	std::priority_queue<OpenCandidate, std::vector<OpenCandidate>, OpenCandidateComparator> OpenSet;
	OpenSet.emplace(std::make_pair(0, Start));

	std::map<Position, Position> CameFrom;

	// smallest cost known from start to n position
	std::map<Position, double> GScore;
	GScore.emplace(Start, 0);

	// fScore hold the heuristic from one point to another (probably the end)
	auto Heuristic = [](Position const& S, Position const& D) { return (D - S).SquaredLength(); };
	std::map<Position, double> FScore;
	FScore.emplace(Start, Heuristic(Start, Dest));

	while (!OpenSet.empty())
	{
		OpenCandidate Candidate = OpenSet.top();
		OpenSet.pop();
		const Position Current = Candidate.second;
		if (Current == Dest)
		{
			return ReconstructPath(CameFrom, Current);
		}

		std::vector<Position> Neighbors = {
			//	Col						Row
			{Mod(Current.X - 1, mWidth), Current.Y},  // Left
			{Mod(Current.X + 1, mWidth), Current.Y},  // Right
			{Current.X, Mod(Current.Y - 1, mHeight)}, // Up
			{Current.X, Mod(Current.Y + 1, mHeight)}  // Down
		};

		for (const Position& Neighbor : Neighbors)
		{
			// Here we can actually reject incorrect neighbor, aka wall;
			Tile const& NeighborTile = GetTile(Neighbor.X, Neighbor.Y);
			if (NeighborTile.mCollision == Collision::CollidesAll)
			{
				continue;
			}
			double TentativeGScore = GScore[Current] + 1; // for now, we only consider the distance between the current and next tile;
			auto&& Search = GScore.find(Neighbor);
			if (Search == GScore.end() || TentativeGScore < Search->second)
			{
				CameFrom.emplace(Neighbor, Current);
				GScore.emplace(Neighbor, TentativeGScore);
				FScore.emplace(Neighbor, TentativeGScore + Heuristic(Neighbor, Dest));
				OpenSet.emplace(std::make_pair(FScore[Neighbor], Neighbor));
			}
		}
	}

	return {};
}
