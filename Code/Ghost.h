#pragma once

#include <utility>

#include "Character.h"
#include "Level.h"

class Renderer;

class Ghost : public Character
{
public:
	Ghost(ECharacter aCharacter);
        ~Ghost();

	void Render(Renderer* aRenderer) const;
	
	virtual bool CanMove(Direction aDirection) const override;

        void SetFleeing(bool aFleeing);

private:
	void SetTilePosition(const std::pair<int, int>& aPosition);

	ECharacter mCharacter;
        bool mFleeing;
};
