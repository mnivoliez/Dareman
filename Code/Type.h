#pragma once

struct Position
{
	int X;
	int Y;

	Position operator-(const Position& Rhs) const;
	Position operator+(const Position& Rhs) const;

	bool operator==(const Position& Rhs) const;
	bool operator<(const Position& Rsh) const;
	bool operator>(const Position& Rsh) const;

	double SquaredLength() const;
	double Length() const;
};
