#pragma once

#include "Character.h"
#include <map>

class Renderer;
class Sprite;

class Dareman : public Character
{
public:
	Dareman();
	~Dareman();
	virtual void Render(Renderer* aRenderer, float aDeltaTime);

        bool IsInvicible() const;
        void UpdateIncibleFrame(float aDeltaTime);
        void BecomeInvicible();

private:	
	Sprite* GetSpriteFromAnimFrameAndDirection(Renderer* aRenderer);

private:
	float mAnimElapseTime;
	bool mFlipAnim;

        bool mIsInvicible;
        float mInvicibleTimer;
};
