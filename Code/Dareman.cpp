#include "Dareman.h"

#include "Renderer.h"
#include "Sprite.h"

constexpr float TIME_FRAME = 0.5f;
constexpr float TIME_INVICIBLE = 5.f;
Dareman::Dareman()
	: Character()
	, mAnimElapseTime(0)
	, mFlipAnim(false)
	, mIsInvicible(false)
	, mInvicibleTimer(0)
{
}

Dareman::~Dareman() {}

void Dareman::Render(Renderer* aRenderer, float aDeltaTime)
{
	mAnimElapseTime += aDeltaTime;
	if (mAnimElapseTime > TIME_FRAME)
	{
		mFlipAnim = !mFlipAnim;
		mAnimElapseTime = 0;
	}
	Sprite* sprite = GetSpriteFromAnimFrameAndDirection(aRenderer);

	if (sprite)
	{
		sprite->SetSize(TILE_SIZE, TILE_SIZE);
		aRenderer->DrawSprite(sprite, (int)mPosX, (int)mPosY);
	}
}

bool Dareman::IsInvicible() const
{
	return mIsInvicible;
}

void Dareman::UpdateIncibleFrame(float aDeltaTime)
{
	if (!mIsInvicible)
	{
		return;
	}

	mInvicibleTimer += aDeltaTime;
	if (mInvicibleTimer >= TIME_INVICIBLE)
	{
		mIsInvicible = false;
		mInvicibleTimer = 0.f;
	}
}
void Dareman::BecomeInvicible()
{
	mIsInvicible = true;
	mInvicibleTimer = 0.f;
}

Sprite* Dareman::GetSpriteFromAnimFrameAndDirection(Renderer* aRenderer)
{
	Sprite* sprite = nullptr;
	if (mFlipAnim)
	{
		switch (mDirection)
		{
		case Up: sprite = aRenderer->GetSprite("Data/Images/pac_u2.png"); break;
		case Down: sprite = aRenderer->GetSprite("Data/Images/pac_d2.png"); break;
		case Left: sprite = aRenderer->GetSprite("Data/Images/pac_l2.png"); break;
		case Right:
		default: sprite = aRenderer->GetSprite("Data/Images/pac_r2.png"); break;
		}
	}
	else
	{
		switch (mDirection)
		{
		case Up: sprite = aRenderer->GetSprite("Data/Images/pac_u1.png"); break;
		case Down: sprite = aRenderer->GetSprite("Data/Images/pac_d1.png"); break;
		case Left: sprite = aRenderer->GetSprite("Data/Images/pac_l1.png"); break;
		case Right:
		default: sprite = aRenderer->GetSprite("Data/Images/pac_r1.png"); break;
		}
	}
	return sprite;
}
