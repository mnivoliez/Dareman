#include "Type.h"
#include <cmath>

Position Position::operator-(const Position& Rhs) const
{
	return { X - Rhs.X, Y - Rhs.Y };
}

Position Position::operator+(const Position& Rhs) const
{
	return Position();
}

bool Position::operator==(const Position& Rhs) const
{
	return X == Rhs.X && Y == Rhs.Y;
}

bool Position::operator<(const Position& Rsh) const
{
	bool bIsOnLowerLine = Y < Rsh.Y;
	bool bIsOnSameLineButLowerCol = Y == Rsh.Y && X < Rsh.X;
	if (bIsOnLowerLine || bIsOnSameLineButLowerCol)
	{
		return true;
	} 
	return false;
}

bool Position::operator>(const Position& Rsh) const
{
	return !(*this<Rsh);
}

double Position::SquaredLength() const
{
	return std::pow(X, 2) + std::pow(Y, 2);
}

double Position::Length() const
{
	return std::sqrt(SquaredLength());
}
