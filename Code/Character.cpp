#include "Character.h"

#include <cassert>

#include "Level.h"

Character::Character()
	: mPosX(0)
	, mPosY(0)
	, mDirection(None)
	, mPendingToBeKilled(false)
{
}

Character::~Character() {}

void Character::SetPosition(int aPosX, int aPosY)
{
	mPosX = (float)aPosX;
	mPosY = (float)aPosY;
}

Position Character::GetLevelPosition() const
{
	return Position{(int)mPosX / TILE_SIZE, (int)mPosY / TILE_SIZE};
}

bool Character::CanMove(Direction aDirection) const
{
	assert(IsOnTile());
	return Level::GetInstance().GetNextTile((int)mPosX / TILE_SIZE, (int)mPosY / TILE_SIZE, aDirection).mCollision == Collision::None;
}

bool Character::IsOnTile() const
{
	return int(mPosX) % TILE_SIZE == 0 && int(mPosY) % TILE_SIZE == 0;
}

float Character::MoveToNextTile(float aDeltaTime)
{
	const float distanceMax = aDeltaTime * GAMEOBJECT_SPEED;

	// Move to next tile, or finish moving into tile in progress
	switch (mDirection)
	{
	case Up:
	{
		int nextTileY = (int)mPosY / TILE_SIZE * TILE_SIZE;
		if (nextTileY == (int)mPosY)
			nextTileY = (int)mPosY - TILE_SIZE;

		const float distanceToNextTile = mPosY - nextTileY;
		if (distanceMax > distanceToNextTile)
		{
			mPosY = (float)nextTileY;
			aDeltaTime -= distanceToNextTile / GAMEOBJECT_SPEED;
		}
		else
		{
			mPosY -= distanceMax;
			aDeltaTime -= distanceMax / GAMEOBJECT_SPEED;
		}
	}
	break;
	case Down:
	{
		const int nextTileY = ((int)mPosY + TILE_SIZE) / TILE_SIZE * TILE_SIZE;
		const float distanceToNextTile = nextTileY - mPosY;
		if (distanceMax > distanceToNextTile)
		{
			mPosY = (float)nextTileY;
			aDeltaTime -= distanceToNextTile / GAMEOBJECT_SPEED;
		}
		else
		{
			mPosY += distanceMax;
			aDeltaTime -= distanceMax / GAMEOBJECT_SPEED;
		}
	}
	break;
	case Right:
	{
		const int nextTileX = ((int)mPosX + TILE_SIZE) / TILE_SIZE * TILE_SIZE;
		const float distanceToNextTile = nextTileX - mPosX;
		if (distanceMax > distanceToNextTile)
		{
			mPosX = (float)nextTileX;
			aDeltaTime -= distanceToNextTile / GAMEOBJECT_SPEED;
		}
		else
		{
			mPosX += distanceMax;
			aDeltaTime -= distanceMax / GAMEOBJECT_SPEED;
		}
	}
	break;
	case Left:
	{
		int nextTileX = (int)mPosX / TILE_SIZE * TILE_SIZE;
		if (nextTileX == (int)mPosX)
			nextTileX = (int)mPosX - TILE_SIZE;

		const float distanceToNextTile = mPosX - nextTileX;

		if (distanceMax > distanceToNextTile)
		{
			mPosX = (float)nextTileX;
			aDeltaTime -= distanceToNextTile / GAMEOBJECT_SPEED;
		}
		else
		{
			mPosX -= distanceMax;
			aDeltaTime -= distanceMax / GAMEOBJECT_SPEED;
		}
	}
	break;
	default: assert(false && "Needs a direction to move"); break;
	}

	return aDeltaTime;
}

void Character::MarkAsToBeKilled()
{
	mPendingToBeKilled = true;
}

bool Character::IsPendingToBeKilled() const
{
	return mPendingToBeKilled;
}
