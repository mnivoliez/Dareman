#include "Ghost.h"

#include <cassert>

#include "Renderer.h"
#include "Sprite.h"

Ghost::Ghost(ECharacter aCharacter)
	: mCharacter(aCharacter)
	, mFleeing(false)
{
	SetTilePosition(Level::GetInstance().GetStartPosition(mCharacter));
}
Ghost::~Ghost() {}

void Ghost::SetTilePosition(const std::pair<int, int>& aPosition)
{
	mPosX = float(aPosition.first * TILE_SIZE);
	mPosY = float(aPosition.second * TILE_SIZE);
}

void Ghost::Render(Renderer* aRenderer) const
{
	Sprite* sprite = nullptr;
	if (mFleeing)
	{
		sprite = aRenderer->GetSprite("Data/Images/ghost_eyes.png");
	}
	else
	{
		switch (mCharacter)
		{
		case ECharacter::Blinky: sprite = aRenderer->GetSprite("Data/Images/blinky.png"); break;
		case ECharacter::Inky: sprite = aRenderer->GetSprite("Data/Images/inky.png"); break;
		case ECharacter::Pinky: sprite = aRenderer->GetSprite("Data/Images/pinky.png"); break;
		case ECharacter::Clyde: sprite = aRenderer->GetSprite("Data/Images/clyde.png"); break;
		default: assert(false && "This character is not a ghost");
		}
	}

	sprite->SetSize(TILE_SIZE, TILE_SIZE);
	aRenderer->DrawSprite(sprite, (int)mPosX, (int)mPosY);
}

bool Ghost::CanMove(Direction aDirection) const
{
	assert(IsOnTile());
	return Level::GetInstance().GetNextTile((int)mPosX / TILE_SIZE, (int)mPosY / TILE_SIZE, aDirection).mCollision != Collision::CollidesAll;
}

void Ghost::SetFleeing(bool aFleeing)
{
	mFleeing = aFleeing;
}
