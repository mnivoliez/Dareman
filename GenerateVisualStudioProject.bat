@echo off
setlocal

set BUILD_DIR=%~dp0Build

if not exist %BUILD_DIR% (
	mkdir %BUILD_DIR%
	cd %BUILD_DIR%
	cmake -G "Visual Studio 17 2022" -A x64 ..
) else (
	cd %BUILD_DIR%
	cmake --debug-output ..
)

pause