# Dareman

Dareman is a poor attempt at replicating a well-known arcade game. Only you can fix it and make it awesome!


# How to build
Dareman uses CMake and SDL in order to support as many platforms, compilers and IDEs as possible.
Scripts and libraries are provided to build on Windows with Visual Studio but it should take little effort to build it on your favorite operating system.

## Windows
- Install Visual Studio 2017
- Install CMake (https://cmake.org/download/) and add cmake to your PATH
- Run GenerateVisualStudioProject.bat
- The solution will be generated in the Build folder
- Build the project
- Binaries will be generated in the Binaries folder
- If you need to add more source files, run GenerateVisualStudioProject.bat again

## linux or macOS
- Install the required packages: cmake, sdl, sdl-image, sdl-ttf
- Generate and build properly, a bat file is provided to generate a simple makefile.
- If you have to modify the CMakeLists.txt, please send us back the modified file to help the next candidates!

# Test instructions

- Optimize the game and maximize the FPS. There are some very obvious bottlenecks and some more subtle ones
- Fix all the memory leaks
- Implement the game logic parts that have been left out:
	- The player must be able to complete the game with the appropriate conditions
	- Implement and use the A* pathfinding algorithm in Level::ComputeShortetPath
	- Features required to be fully functional: player controls, pickups, ghost chase, flee and respawn logic
	- Visual polish: dareman must be animated, ghost statuses must be displayed
	- Optional: Fully implement the game logic of the original arcade game. Some more information can be found online, for example: http://gameinternals.com/post/2072558330/understanding-pac-man-ghost-behavior
- Refactoring the code, moving parts around and adding new files is encouraged. We left some bad patterns in there intentionally, if something looks wrong to you, show us how you would have made it better
- Document your changes and refactors in this file. Suggestions are encouraged even if they were not implemented.
- Your code must be easily readable and commented appropriately
- Usage of modern C++ and best practices is encouraged

# Test results

- Write your comments here...
- Assuming you are using git to track your changes, package the test using the following command before sending it back: `git archive --format="zip" --output="dareman_yourname.zip" HEAD`


- Sadly, I didn't have as mush time as I would have like. I spend about 8.5h on the project.
- What have been done:
  - Fix the repeated load of sprite by storing them into a map in the renderer.
  - Add score
  - Make the ghost follow dareman using the AStar function in level
  - Create a position type for convenience
  - Animate Dareman
  - I think I found most of the memory leak if not all
  - Add the fleing behaviour and the incibility buf to dareman
- What I didn't have time to do:
  - The respawn logic
   - Probably reset the level
  - The original behaviour of the ghost.
- If given more time here what I would do:
  - Refactor the game into a proper class to have main as clean as possible.
  - Create a rendable interface and make all rendable object implement it.
  - Maybe introduce a FlipBook system for animation.
  - Add an asset manager to load any asset
  - Refactor any use of std::pair<int, int> or of aCol/aRow to a position.
  - Annihilate all nesting that goes beyond 3.
